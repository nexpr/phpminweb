<?php

namespace phpminweb;

class App {
    static function run() {
        $app = new self();
        [$path] = explode('?', $_SERVER['REQUEST_URI'], 2);
        $app->router->run($path);
    }

    private $_instances;
    public $ctx;

    function __construct() {
        $this->_instances = new \stdClass();
        $this->ctx = new \stdClass();
    }

    function __get($name) {
        if (property_exists($this->_instances, $name)) {
            return $this->_instances->$name;
        }
        $class = 'phpminweb\\App\\' . ucfirst($name);
        $instance = new $class($this);
        $this->_instances->$name = $instance;
        return $instance;
    }
}
