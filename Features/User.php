<?php

namespace phpminweb\Features;

class User {
    function __construct(private $db) {
    }

    function register($username, $password, $permissions){
        $hashed_password = password_hash($password, PASSWORD_DEFAULT);

        $this->db->tx(function () use ($username, $hashed_password, $permissions) {
            $this->db->query('
                insert into user (username, hashed_password) values (?, ?)
            ', [$username, $hashed_password]);

            $id = $this->db->getLastId();

            if (is_array($permissions) && count($permissions) > 0) {
                $params = array_map(fn($permission) => [$id, $permission], $permissions);
                $query_placeholder = join(',', array_map(fn($x) => '(?,?)', $params));
                $params_flat = array_reduce($params, fn($a, $b) => array_merge($a, $b), []);
                $this->db->query('
                    insert into user_permission (user_id, permission)
                    values ' . $query_placeholder . '
                ', $params_flat);
            }
        });
    }
}
