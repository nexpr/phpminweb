<?php

namespace phpminweb\Features;

class Item {
    function __construct(private $db) {
    }

    function byId($id) {
        $rows = $this->db->query('
            select * from item where id = ?
        ', [$id]);
        return $rows[0] ?? null;
    }

    function select($query) {
        $wheres = [];
        $params = [];

        if ($query) {
            if ($query->text) {
                $wheres[] = 'body like ?';
                $params[] = '%' . $query->text . '%';
            }
            if ($query->from) {
                $wheres[] = 'ts >= ?';
                $params[] = $query->from->format('Y-m-d H:i:s');
            }
            if ($query->to) {
                $wheres[] = 'ts < ?';
                $params[] = $query->from->format('Y-m-d H:i:s');
            }
            if ($query->user_id) {
                $wheres[] = 'user_id = ?';
                $params[] = $query->user_id;
            }
        }

        $where = count($wheres) > 0 ? join(' AND ', $wheres) : 'true';

        $rows = $this->db->query('
            select item.*, user.username from item
            join user on item.user_id = user.id
            where ' . $where . '
        ', $params);
        return $rows;
    }

    function insert($body, $user_id) {
        $this->db->query('
            insert into item (body, ts, user_id) values (?, now(), ?)
        ', [$body, $user_id]);
    }

    function update($item_id, $body, $user_id) {
        $this->db->query('
            update item set body = ?, ts = now()
            where id = ? and user_id = ?
        ', [$body, $item_id, $user_id]);
    }

    function delete($item_id, $user_id) {
        $this->db->query('
            delete from item where id = ? and user_id = ?
        ', [$item_id, $user_id]);
    }
}
