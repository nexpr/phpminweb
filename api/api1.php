<?php

return [
    function($app) {
        $app->ctx->data = [
            'a' => 100,
            'b' => 200,
        ];
    },
    ['json', 'data'],
];
