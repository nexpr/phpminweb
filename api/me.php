<?php

return [
    ['validate', [
        'method' => 'GET',
    ]],
    function($app) {
        $app->response->json($app->user->getInfo());
        return true;
    },
];
