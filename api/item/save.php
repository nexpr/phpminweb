<?php

use phpminweb\Features\Item;

return [
    ['permission'],
    ['validate', [
        'method' => 'POST',
        'body' => [
            ['object', [
                'keys' => [
                    'id' => [['null'], ['int', ['min' => 1]]],
                    'body' => ['string', ['min' => 1]],
                ],
            ]],
        ],
    ]],
    function($app) {
        $item_id = $app->ctx->body->id;
        $body = $app->ctx->body->body;
        
        $user_id = $app->user->getInfo()->id;
        $item = new Item($app->db);
        if ($item_id) {
            $item->update($item_id, $body, $user_id);
        } else {
            $item->insert($body, $user_id);
        }

        $app->ctx->data = ['success' => true];
    },
    ['json', 'data'],
];
