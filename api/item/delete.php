<?php

use phpminweb\Features\Item;

return [
    ['permission'],
    ['validate', [
        'method' => 'POST',
        'body' => [
            ['object', [
                'keys' => [
                    'id' => ['int', ['min' => 1]],
                ],
            ]],
        ],
    ]],
    function($app) {
        $item_id = $app->ctx->body->id;
        
        $user_id = $app->user->getInfo()->id;
        $item = new Item($app->db);
        $item->delete($item_id, $user_id);

        $app->ctx->data = ['success' => true];
    },
    ['json', 'data'],
];
