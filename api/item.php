<?php

use phpminweb\Features\Item;

return [
    ['validate', [
        'method' => 'GET',
        'query' => ['int', ['min' => 1]],
    ]],
    function($app) {
        $item = new Item($app->db);
        $app->ctx->data = $item->byId($app->ctx->query);
    },
    ['json', 'data'],
];
