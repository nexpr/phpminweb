<?php

return [
    ['validate', [
        'method' => 'POST',
        'body' => ['object', [
            'keys' => [
                'username' => ['string'],
                'password' => ['string'],
            ],
        ]],
    ]],
    function($app) {
        $username = $app->ctx->body->username;
        $password = $app->ctx->body->password;

        $success = $app->user->login($username, $password);
        $app->response->json(['success' => $success]);
        return true;
    },
];
