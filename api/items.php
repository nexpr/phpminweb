<?php

use phpminweb\Features\Item;

return [
    ['validate', [
        'method' => 'GET',
        'query' => [
            ['null'],
            ['object', [
                'keys' => [
                    'text' => [['null'], ['string', ['min' => 1]]],
                    'from' => [['null'], ['date']],
                    'to' => [['null'], ['date']],
                    'user_id' => [['null'], ['int', ['min' => 1]]],
                ],
            ]],
        ],
    ]],
    function($app) {
        $item = new Item($app->db);
        $app->ctx->data = $item->select($app->ctx->query);
    },
    ['json', 'data'],
];
