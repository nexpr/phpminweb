<?php

return [
    ['validate', [
        'method' => 'POST',
    ]],
    function($app) {
        $app->user->logout();

        $app->response->json(['success' => true]);
        return true;
    },
];
