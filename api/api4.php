<?php

use phpminweb\Features\Calc;

return [
    ['validate', [
        'method' => 'GET',
        'query' => ['array', [
            'values' => ['object', [
                'keys' => [
                    'type' => ['eq', ['values' => ['add', 'sub']]],
                    'value' => ['int', ['min' => 1]],
                ],
            ]],
        ]],
    ]],
    function($app) {
        $c = new Calc(0);
        foreach($app->ctx->query as $v) {
            if ($v->type === 'add') {
                $c->add($v->value);
            } else if ($v->type === 'sub') {
                $c->sub($v->value);
            }
        }

        $app->ctx->data = $c->get();
    },
    ['json', 'data'],
];

/*
/api/api4?query=[{"type":"add","value":1},{"type":"sub","value":3}]

=> -2
*/