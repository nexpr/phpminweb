<?php

return [
    ['validate', [
        'method' => 'GET',
        'query' => ['object', [
            'keys' => [
                'a' => ['int'],
                'b' => ['string'],
            ],
        ]],
    ]],
    function($app) {
        $app->ctx->data = [
            'a' => $app->ctx->query->a,
            'b' => $app->ctx->query->b,
        ];
    },
    ['json', 'data'],
];
