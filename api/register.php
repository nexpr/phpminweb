<?php

use phpminweb\Features\User;

return [
    ['validate', [
        'method' => 'POST',
        'body' => ['object', [
            'keys' => [
                'username' => ['string'],
                'password' => ['string'],
                'permissions' => ['array', ['values' => ['string']]],
            ],
        ]],
    ]],
    function($app) {
        $username = $app->ctx->body->username;
        $password = $app->ctx->body->password;
        $permissions = $app->ctx->body->permissions;

        $user = new User($app->db);
        $user->register($username, $password, $permissions);

        $app->response->json(['success' => true]);
        return true;
    },
];
