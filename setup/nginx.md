nginx の設定フォルダに .conf ファイルを追加

```
server {
    listen 3999;
    root /opt/phpminweb/static;
    index index.html;

    location / {
        try_files $uri $uri/index.html @php;
    }

    location @php {
        fastcgi_pass   unix:/run/php/php8.1-fpm.sock;
        fastcgi_param  SCRIPT_FILENAME  /opt/phpminweb/boot.php;
        include        fastcgi_params;
    }
}
```

環境に応じて変更

- ポート
- phpminweb フォルダのパス
- sock ファイルのパス
