<?php

return [
    ['permission', 'page3'],
    function($app) {
        $app->ctx->data = [
            'a' => 10,
            'b' => 20,
        ];
    },
    ['template', 'page3'],
];
