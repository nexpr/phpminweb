<?php

return [
    function($app) {
        $sess = $app->csession->get();
        
        $count = $sess->count ?? 0;
        $app->ctx->count = $count;
        $sess->count = $count + 1;

        $app->csession->set($sess);
    },
    ['template', 'page4'],
];
