<?php

namespace phpminweb\App;

use phpminweb\App\AppClasses;

class Env extends AppClasses {
    private $_env = null;

    function __get($name) {
        if (!$this->_env) {
            $this->_env = $this->getEnv();
        }
        return $this->_env->$name;
    }

    function getEnv() {
        $env = getenv('PHPMINWEB_ENV');
        $dir = $this->getEnvDir();
        $result = null;

        if ($env) {
            $result = $this->readJSON("{$dir}/{$env}.json");
        }
        if (!$result) {
            $result = $this->readJSON("{$dir}/default.json");
        }
        if (!$result) {
            throw new \Exception('Invalid Env');
        }

        return $result;
    }

    function getEnvDir() {
        $dir = getenv('PHPMINWEB_ENV_DIR');
        if ($dir && is_dir($dir)) {
            return $dir;
        } else {
            return __DIR__ . '/../env';
        }
    }

    function readJSON(string $filename) {
        $json = file_get_contents($filename);
        if (!$json) return null;
        return json_decode($json);
    }
}