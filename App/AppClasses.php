<?php

namespace phpminweb\App;

use phpminweb\App;

class AppClasses {
    protected App $app;

    function __construct(App $app) {
        $this->app = $app;
    }
}
