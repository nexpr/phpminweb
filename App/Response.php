<?php

namespace phpminweb\App;

use phpminweb\App\AppClasses;

class Response extends AppClasses {
    function html($html, $status = 200) {
        http_response_code($status);
        header('Content-Type: text/html');
        echo $html;
    }

    function json($value, $status = 200) {
        http_response_code($status);
        header('Content-Type: application/json');
        echo json_encode($value);
    }

    function file($filepath, $status = 200) {
        http_response_code($status);
        readfile($filepath);
    }

    function apiNotFound() {
        $this->json(['error' => 'api not found']);
    }

    function apiValidationError($ex) {
        $this->json(['error' => 'api validation error', 'detail' => $ex->detail]);
    }

    function apiPermissionError($ex) {
        $this->json(['error' => 'api permission error']);
    }

    function apiError($ex) {
        $this->json(['error' => 'api error']);
    }

    function pageNotFound() {
        $this->file($this->app->constant->default_html);
    }

    function pageValidationError($ex) {
        $this->html('
            <h1>Validation error</h1>
            <pre>' . json_encode($ex->detail) .'</pre>
        ', 400);
    }

    function pagePermissionError($ex) {
        $this->html('
            <h1>Permission error</h1>
        ', 403);
    }

    function pageError($ex) {
        $this->html('
            <h1>Error</h1>
            <pre>' . $ex .'</pre>
        ', 500);
    }
}
