<?php

namespace phpminweb\App;

use phpminweb\App\AppClasses;

class Constant extends AppClasses {
    private array $constants;

    function __construct() {
        $this->constants = [
            'api_dir' => __DIR__ . '/../api',
            'pages_dir' => __DIR__ . '/../pages',
            'templates_dir' => __DIR__ . '/../templates',
            'default_html' => __DIR__ . '/../resources/default.html',
        ];
    }

    function __get($name) {
        return $this->constants[$name];
    }
}