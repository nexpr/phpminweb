<?php

namespace phpminweb\App\Exception;

class ValidationException extends \Exception {
    function __construct(public $type, public $detail) {
        parent::__construct("validation error: $type");
    }
}