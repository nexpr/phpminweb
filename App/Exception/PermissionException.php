<?php

namespace phpminweb\App\Exception;

class PermissionException extends \Exception {
    function __construct() {
        parent::__construct("permission error");
    }
}