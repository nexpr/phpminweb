<?php

namespace phpminweb\App;

use phpminweb\App\AppClasses;
use phpminweb\App\Request\ObjectValidator;
use phpminweb\App\Exception\ValidationException;

class Request extends AppClasses {
    function validateMethod($required_method) {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method !== $required_method) {
            throw new ValidationException('method', ['method' => $method]);
        }
    }

    function validateCSRF() {
        $headers = getallheaders();
        if (!in_array($headers['Sec-Fetch-Site'], ['same-origin', 'none'])) {
            throw new ValidationException('csrf', ['headers' => $headers]);
        }
    }

    function validateQuery($option) {
        $json = $_GET['query'] ?? null;
        $query = json_decode($json);

        $validator = new ObjectValidator($query);
        $result = $validator->validate($option);
        if (!$result['valid']) {
            throw new ValidationException('query', $result);
        }
        return $result['value'];
    }

    function validateBody($option) {
        $json = file_get_contents('php://input');
        $body = json_decode($json);

        $validator = new ObjectValidator($body);
        $result = $validator->validate($option);
        if (!$result['valid']) {
            throw new ValidationException('body', $result);
        }
        return $result['value'];
    }
}
