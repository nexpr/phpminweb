<?php

namespace phpminweb\App;

use phpminweb\App\AppClasses;

class User extends AppClasses {
    private $_user;

    function __construct($app) {
        parent::__construct($app);
        $sess = $this->app->csession->get();
        $id = $sess->user ?? null;
        if ($id) {
            $this->_user = $this->getUser($id);
        }
    }

    function getUser($id){
        $rows = $this->app->db->query('
            SELECT * FROM user WHERE id = ?
        ', [$id]);

        $user = $rows[0] ?? null;
        if (!$user) return;

        $rows = $this->app->db->query('
            SELECT permission FROM user_permission WHERE user_id = ?
        ', [$user->id]);

        $user->permissions = array_map(fn($a) => $a->permission, $rows);

        return $user;
    }

    function login($username, $password) {
        $id = $this->authenticate($username, $password);
        if ($id === null) return false;

        $sess = $this->app->csession->get();
        $sess->user = $id;
        $this->app->csession->set($sess);

        return true;
    }

    function logout() {
        $sess = $this->app->csession->get();
        $sess->user = null;
        $this->app->csession->set($sess);
    }

    function authenticate($username, $password) {
        $rows = $this->app->db->query('
            SELECT id, hashed_password FROM user WHERE username = ?
        ', [$username]);

        $user = $rows[0] ?? null;

        if (!$user || !password_verify($password, $user->hashed_password)) {
            return null;
        }

        return $user->id;
    }

    function getInfo() {
        return $this->_user === null
            ? null
            : (object)['id' => $this->_user->id, 'name' => $this->_user->username];
    }

    function hasPermission($permission) {
        if (!$this->_user) {
            return false;
        }
        return in_array($permission, $this->_user->permissions, true);
    }
}
