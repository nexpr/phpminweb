<?php

namespace phpminweb\App;

use phpminweb\App\AppClasses;
use phpminweb\App\Exception\PermissionException;
use phpminweb\App\Exception\ValidationException;

class Router extends AppClasses {
    function run($path) {
        if (str_starts_with($path, '/api/')) {
            $filepath = $this->app->constant->api_dir . substr($path, strlen('/api')) . '.php';
            $this->route($filepath, 'api');
        } else {
            $filepath = $this->app->constant->pages_dir . $path . '.php';
            $this->route($filepath, 'page');
        }
    }

    function route($filepath, $type) {
        if (!is_file($filepath)) {
            if ($type === 'api') {
                $this->app->response->apiNotFound();
            } else {
                $this->app->response->pageNotFound();
            }
            return;
        }

        try {
            $def = require_once $filepath;
            $this->app->route->run($def);
        } catch (PermissionException $ex) {
            if ($type === 'api') {
                $this->app->response->apiPermissionError($ex);
            } else {
                $this->app->response->pagePermissionError($ex);
            }
        } catch (ValidationException $ex) {
            if ($type === 'api') {
                $this->app->response->apiValidationError($ex);
            } else {
                $this->app->response->pageValidationError($ex);
            }
        } catch (\Exception $ex) {
            error_log($ex);
            if ($type === 'api') {
                $this->app->response->apiError($ex);
            } else {
                $this->app->response->pageError($ex);
            }
        }
    }
}