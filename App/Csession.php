<?php

namespace phpminweb\App;

use phpminweb\App\AppClasses;

class Csession extends AppClasses {
    const method = 'aes-256-cbc';

    private $cache = null;

    function get() {
        if ($this->cache) {
            return $this->cache;
        }

        $cookie = $this->app->env->csession_cookie;
        $key = $this->app->env->csession_key;

        $value = @$_COOKIE[$cookie];
        if (!$value) return new \stdClass();

        [$iv_b64, $encrypted] = explode('|', $value, 2);
        $iv = base64_decode($iv_b64);

        $json = openssl_decrypt($encrypted, self::method, $key, 0, $iv);
        if (!$json) return new \stdClass();

        $data = json_decode($json);
        $this->cache = $data;

        return $data;
    }

    function set($data) {
        $cookie = $this->app->env->csession_cookie;
        $key = $this->app->env->csession_key;
        $iv = openssl_random_pseudo_bytes(16);
        $encrypted = openssl_encrypt(json_encode($data), self::method, $key, 0, $iv);
        $value = base64_encode($iv) . '|' . $encrypted;
        setcookie($cookie, $value, 0, '/', '', false, true);
    }
}
