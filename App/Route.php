<?php

namespace phpminweb\App;

use phpminweb\App\AppClasses;
use phpminweb\App\Exception\PermissionException;

class Route extends AppClasses {
    function run($def) {
        foreach ($def as $value) {
            $break = false;
            if (is_callable($value)) {
                $break = $value($this->app);
            } else if (is_array($value)) {
                $method = $value[0];
                $break = $this->$method($value[1] ?? null);
            }
            if ($break) break;
        }
    }

    function validate($option) {
        $req = $this->app->request;

        if ($option['method'] ?? null !== null) {
            $req->validateMethod($option['method']);
        }
        if ($option['csrf'] ?? true !== false) {
            $req->validateCSRF();
        }
        if ($option['query'] ?? null) {
            $this->app->ctx->query = $req->validateQuery($option['query']);
        }
        if ($option['body'] ?? null) {
            $this->app->ctx->body = $req->validateBody($option['body']);
        }
    }

    function permission($permission) {
        if ($permission) {
            if (!$this->app->user->hasPermission($permission)) {
                throw new PermissionException();
            }
        } else {
            if (!$this->app->user->getInfo()) {
                throw new PermissionException();
            }
        }
    }

    function template($name) {
        $ctx = $this->app->ctx;
        ob_start();
        try {
            require $this->app->constant->templates_dir . '/' . $name . '.php';
        } finally {
            $text = ob_get_clean();
        }
        $this->app->response->html($text);
    }

    function json($key) {
        $this->app->response->json($this->app->ctx->$key);
    }
}
