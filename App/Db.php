<?php

namespace phpminweb\App;

use phpminweb\App\AppClasses;

class Db extends AppClasses {
    public \PDO $pdo;

    function __construct($app) {
        parent::__construct($app);
        $dbconfig = $this->app->env->db;
        $this->pdo = new \PDO(
            $dbconfig->dsn,
            $dbconfig->username,
            $dbconfig->password
        );
    }

    function query($sql, $params = []) {
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($params);
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }

    function getLastId() {
        return $this->pdo->lastInsertId();
    }

    function tx($fn) {
        $this->pdo->beginTransaction();
        try {
            $fn();
            $this->pdo->commit();
        } finally {
            if ($this->pdo->inTransaction()) {
                $this->pdo->rollBack();
            }
        }
    }
}
